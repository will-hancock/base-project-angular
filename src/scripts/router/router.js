﻿// Router
App.config(['$routeProvider', function ($routeProvider) {
	$routeProvider.
	when('/', {
		templateUrl: 'views/index.html',
		controller: 'indexCtrl'
	}).
	otherwise({
		redirectTo: '/'
	});
}]);