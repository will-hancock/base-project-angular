﻿// Directive
App.directive('aDirective', function () {
	return {
		restrict: 'E',
		replace: 'true',
		templateUrl: '../views/partials/template.html',
		controller: 'directiveCtrl'
	};
});