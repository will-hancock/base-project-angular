﻿# Views

This views directory contains the partials and markup for Angular views.

For static HTML 'templates' look in ```../templates```