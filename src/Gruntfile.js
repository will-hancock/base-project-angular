/*global module:true */
module.exports = function ( grunt ) {
	'use strict';

	// Array of Script files to include into build
	var APP_SCRIPTS = [
		// module dependancies
		'scripts/modules/*.js',

		// app
		'scripts/app.js',

		// services
		'scripts/services/*.js',

		// controllers
		'scripts/controllers/*.js',

		// directives
		'scripts/directives/*.js',

		// router - goes last
		'scripts/router/*.js',
		'scripts/libs/angular-route-1.2.22.min.js'
	];

	require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);
	require('matchdep').filterDev('assemble').forEach(grunt.loadNpmTasks);

	// GRUNT PLUGIN CONFIGURATION
	grunt.initConfig({

		// Get package data
		pkg: grunt.file.readJSON( 'package.json' ),

		// Banners to prepend to built files
		banner: {
			ecma: '/**\n' +
					' *  <%= pkg.title %> - v<%= pkg.version %> - <%= grunt.template.today("yyyy-mm-dd") %>\n' +
					'<%= pkg.homepage ? " *  " + pkg.homepage + "\\n" : "" %>' +
					' *  Copyright (c)<%= grunt.template.today("yyyy") %> <%= pkg.author.name %>\n' +
					' */ \n\n'
		},

		// JSHINT 
		jshint: { /* Lint the Gruntfile, all modules and specs except JQuery. */
			all: [
                'Gruntfile.js',
                'scripts/**/*.js',

                // Nots
                '!scripts/libs/**/*.js'
			],
			options: {
				bitwise: false,
				browser: true,
				curly: true,
				eqeqeq: true,
				es3: true, /* Assume site must work in IE6/7/8. */
				forin: true,
				globals: { /* Require and Jasmine's Globals. Note that $ is not permitted. */
					'requirejs': false, /* Require */
					'require': false,
					'define': false,
					'describe': false, /* Jasmine */
					'xdescribe': false,
					'it': false,
					'xit': false,
					'beforeEach': false,
					'afterEach': false,
					'jasmine': false,
					'spyOn': false,
					'expect': false,
					'waitsFor': false
				},
				immed: true,
				indent: 4,
				jquery: true,
				latedef: true,
				maxdepth: 2,
				newcap: true,
				noarg: true,
				noempty: false,
				onevar: true,
				plusplus: false,
				quotmark: 'single', /* Use backslashes if they\'re needed. */
				regexp: false,
				regexdash: true,
				'-W099': true, // suppresses mixed spaces and tabs errors - have both.
				strict: false, /* Use one 'use strict'; per file. */
				trailing: true, /* Turn 'show whitespace' on in your editor. */
				undef: false,
				unused: false
			}
		},

		// CLEAN Output dir
		clean: {
			options: {
				force: true
			},
			src: ['../dist/']
		},

		// COPY 
		copy: {
			fonts: {
				src: 'fonts/**',
				dest: '../dist/'
			},
			scripts: {
				src: 'scripts/libs/**',
				dest: '../dist/'
			},
			images: {
				src: 'images/**',
				dest: '../dist/'
			},
			views: {
				src: 'views/**',
				dest: '../dist/'
			}
		},

		// SASS COMPILATION
		sass: {
			options: {
				banner: '<%= banner.ecma %>'
			},

			dev: {
				files: {
					'../dist/styles/main.css': 'styles/main.scss'
				},
				options: {
					style: 'expanded',
					lineNumbers: true
				}
			},
			prod: {
				files: {
					'../dist/styles/main.css': 'styles/main.scss'
				},
				options: {
					style: 'compressed'
				}
			}
		},

		// FILE CONCATINATION 
		concat: {
			options: {
				banner: '<%= banner.ecma %>'
			},
			scripts: {
				src: APP_SCRIPTS,
				dest: '../dist/scripts/app.js'
			}
		},

		// SCRIPT MINIFICATION
		uglify: {
			options: {
				banner: '<%= banner.ecma %>'
			},
			scripts: {
				files: {
					'../dist/scripts/app.js': APP_SCRIPTS
				}
			}
		},

		// ASSEMBLE THE MARKUP
		assemble: {
			options: {
				assets: '../dist',
				data: ['../data/*.{json,yml}', 'package.json'],
				partials: 'templates/modules/*.hbs',
				ext: '.html',
				helpers: 'templates/helpers/*.js',
				layout: 'templates/layout/master.hbs',
				removeHbsWhitespace: true
			},
			pages: {
				options: {
					production: false
				},
				files: [{
					expand: true,
					cwd: 'templates/pages',
					src: ['*.hbs'],
					dest: '../dist'
				}]
			}
		},

		// WATCH for file changes
		watch: {
			options: {
				livereload: true
			},
			data: {
				files: ['../data/**/*.json'],
				tasks: ['build-dev'],
				options: {
					spawn: false
				}
			},
			configuration: {
				files: ['Gruntfile.js', '**/*.json'],
				tasks: ['build-dev'],
				options: {
					spawn: false
				}
			},
			images: {
				files: ['images/**'],
				tasks: ['copy:images'],
				options: {
					spawn: false
				}
			},
			scripts: {
				files: ['scripts/**/*.js'],
				tasks: ['concat:scripts'],
				options: {
					spawn: false
				}
			},
			styles: {
				files: ['styles/**/*.{scss,sass}'],
				tasks: ['sass:dev'],
				options: {
					spawn: true
				}
			},
			markup: {
				files: ['templates/**/*.hbs', 'templates/**/*.js'],
				tasks: ['assemble'],
				options: {
					spawn: false
				}
			},
			views: {
				files: ['views/**/*.html'],
				tasks: ['copy:views'],
				options: {
					spawn: false
				}
			}
		}
	} );

	// ENVIRONMENT TASKS

	// development build - run 'grunt dev'
	grunt.registerTask( 'dev', ['clean', 'build-dev'] );
	grunt.registerTask( 'build-dev', ['sass:dev', 'assemble', 'copy', 'concat:scripts', 'jshint'] );

	// production build - run 'grunt prod'
	grunt.registerTask( 'prod', ['clean', 'build-prod'] );
	grunt.registerTask( 'build-prod', ['sass:prod', 'copy', 'uglify', 'assemble'] );


	// default task - Production to prevent development code going live
	grunt.registerTask( 'default', 'prod' );
};
